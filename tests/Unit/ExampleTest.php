<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        //$this->assertTrue(true);

        $this->artisan('consoletest')
        ->expextsQquestion('What is your name?','test')
        ->expextsQquestion('Which language do you program in?','java')
        ->expextsOutput('Your name is test and you program in java')
        ->assertTrue(0);
    }
}
