<?php
use App\Notifications\TestNotification;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('check',function () {
    return action([HomeController::class,'index']);
});

//Auth::routes();
Route::get('send-notification', function () {
    auth()->user()->notify((new TestNotification)->locale('fr'));
  });


Auth::routes(['verify' => true]);

Route::get('profile', function () {
    return "this is your profile";
})->middleware('verified');


Route::get('/home-page', 'HomeController@index')->name('home');


